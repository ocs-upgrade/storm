%define debug_package %{nil}
%define _prefix /opt
%define _conf_dir %{_sysconfdir}/storm
%define _log_dir %{_var}/log/storm
%define _data_dir %{_sharedstatedir}/storm


Summary:        Apache Storm is a free and open source distributed realtime computation system
Name:           storm
Version:        2.4.0
Release:        7%{?dist}
License:        Apache-2.0
URL:            http://storm.apache.org
Source0:        http://archive.apache.org/dist/%{name}/apache-%{name}-%{version}/apache-%{name}-%{version}.tar.gz
Source1:        %{name}-nimbus.service
Source2:        %{name}-supervisor.service
Source3:        %{name}-logviewer.service
Source4:        %{name}-ui.service
Source5:        %{name}.logrotate
Source6:        %{name}.conf

BuildRequires:  systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Requires: java

%description
Apache Storm is a free and open source distributed realtime computation system. Apache Storm makes it easy to reliably process unbounded streams of data, 
doing for realtime processing what Hadoop did for batch processing. Apache Storm is simple, can be used with any programming language, and is a lot of fun to use!
It has many use cases: realtime analytics, online machine learning, continuous computation, distributed RPC, ETL, and more. Apache Storm is fast: a benchmark clocked it at over a million tuples processed per second per node. 
It is scalable, fault-tolerant, guarantees your data will be processed, and is easy to set up and operate.
It integrates with the queueing and database technologies you already use. An Apache Storm topology consumes streams of data and processes those streams in arbitrarily complex ways, 
repartitioning the streams between each stage of the computation however needed. Read more in the tutorial.

%prep
%autosetup -n apache-%{name}-%{version}
sed -i 's/\!\/usr\/bin\/env python/\!\/usr\/bin\/env python3/g' bin/storm.py

%build

%install
mkdir -p %{buildroot}%{_log_dir}
mkdir -p %{buildroot}%{_data_dir}
mkdir -p %{buildroot}%{_conf_dir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_prefix}/storm/
cp -pr bin %{buildroot}%{_prefix}/storm/
cp -pr lib %{buildroot}%{_prefix}/storm/
cp -pr external %{buildroot}%{_prefix}/storm/
cp -pr log4j2 %{buildroot}%{_prefix}/storm/
cp -pr public %{buildroot}%{_prefix}/storm/
cp -pr extlib %{buildroot}%{_prefix}/storm/
cp -pr extlib-daemon %{buildroot}%{_prefix}/storm/
install -p -D -m 755 %{S:1} %{buildroot}%{_unitdir}/
install -p -D -m 755 %{S:2} %{buildroot}%{_unitdir}/
install -p -D -m 755 %{S:3} %{buildroot}%{_unitdir}/
install -p -D -m 755 %{S:4} %{buildroot}%{_unitdir}/
install -p -D -m 644 %{S:5} %{buildroot}%{_sysconfdir}/logrotate.d/storm
install -p -D -m 644 %{S:6} %{buildroot}%{_sysconfdir}/sysconfig/storm
install -p -D -m 644 conf/storm.yaml %{buildroot}%{_conf_dir}/
install -p -D -m 644 conf/storm_env.ini %{buildroot}%{_conf_dir}/

%post
%systemd_post storm-nimbus.service
%systemd_post storm-supervisor.service
%systemd_post storm-logviewer.service
%systemd_post storm-ui.service

%preun
for module in nimbus supervisor logviewer ui; do
systemctl status storm-$module > /dev/null
if [[ $? -eq 0 ]]; then
%systemd_preun storm-$module.service
fi
done

%files
%license LICENSE
%doc SECURITY.md README.markdown NOTICE
%defattr(-,root,root)
%{_unitdir}/storm-nimbus.service
%{_unitdir}/storm-supervisor.service
%{_unitdir}/storm-logviewer.service
%{_unitdir}/storm-ui.service
%config(noreplace) %{_sysconfdir}/logrotate.d/storm
%config(noreplace) %{_sysconfdir}/sysconfig/storm
%config(noreplace) %{_conf_dir}/*
%{_prefix}/storm
%dir %{_log_dir}
%dir %{_data_dir}

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-6
- Rebuilt for loongarch release

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-5
- Rebuilt for loongarch release

* Wed Nov 29 2023 Feng Weiyao <wynnfeng@tencent.com> - 2.4.0-4
- add %{?dist} after release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Apr 12 2023 Feng Weiyao <wynnfeng@tencent.com> - 2.4.0-1
- initial build
